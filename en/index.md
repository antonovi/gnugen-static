---
layout: page
lang: en
permalink: /en/
---

Welcome to __GNU Generation's__ website! Our mission is to promote the use of [Free Software](https://gnu.org/philosopy) on the [EPFL](https://www.epfl.ch/) campus. You can find more information about our missions [here](/en/about/).

# Future events

## the _Geekerie_

We organize a geekerie this semester, you can send a mail to
[subscribe+annonces@lists.gnugen.ch](subscribe+annonces@lists.gnugen.ch) to be
notified when the date will be confirmed.

We hope to see you there!
