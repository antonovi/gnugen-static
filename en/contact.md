---
layout: page
lang: en
permalink: /en/contact/
---

## Room

We make use of a local in  [**CM 0 415**](https://plan.epfl.ch/?room=CM+0+415) in which you will almost always find a member who will be happy to help you. You can even pass by weekends!

## Chat

You can also join us in our main chat room which is bridged between Matrix and IRC:

 * [on the web](https://web.libera.chat/?channel=#gnugen), or `irc.libera.chat` port 6697, #gnugen from an IRC client;
 * for the lucky ones that may have a matrix server: [#main:gnugen.ch](https://matrix.to/#/#main:gnugen.ch).

(As a member of the association you have access to a chat account on our Matrix server.)

## Weekly meetings

We hold [meetings every two weeks](https://wiki.gnugen.ch/reunions/accueil) during the semester.

## E-mails and address

You can contact us by e-mail at **contact (at) gnugen.ch** or individual members of the comitee at:

  * **president (at) gnugen.ch** for the president, currently *Matthieu De Beule*.
  * **vicepresident (at) gnugen.ch** for the vice-president, currently *Ulysse Widmer*.
  * **tresorier (at) gnugen.ch** for the treasurer, currently *Haley Owsianko*.
  * **informatique (at) gnugen.ch** for the IT guys, currently *Joachim Desroches* and
    *Antoine Fontaine*.

We also have a postal address:

```
AGEPoly - GNU Generation
P.a. AGEPoly
Case postale 16
CH-1015 Lausanne
```

