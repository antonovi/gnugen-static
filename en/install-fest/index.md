---
layout: page
lang: en
permalink: /en/install-fest/
---

# Install Fests

This semester, the Install Fest **took place** on October 10th, in [CM 1 106](https://map.epfl.ch/?room=CM%201%20106).

![Affiche de l'Install-Fest du 10 Octobre 2021](/assets/images/posts/install-fest-fall-2021.png)

## Install Fest? What's that?

An Install Fest is an even organized by the [GNU Generation](/en) where you can find people both able and motivated to
help you install and use GNU/Linux or any other free software.

You will also find people interested in computers in general, free software and other geeky topics meeting each other
and sharing knowledge or fun experiences around small presentations.

<!--
Some other common activities happening during Install Fests include:

- [PGP Keysigning Party](http://linuxreviews.org/howtos/gnupg/signingparty/) to sign [PGP keys](https://gnupg.org)
- Presentations of some free software
- ~~[Potluck](https://en.wikipedia.org/wiki/Potluck) :D~~ sadly not this year
-->

## When and where?

**See you next semester !** You can send a mail to
[annonces+subscribe@lists.gnugen.ch](annonces+subscribe@lists.gnugen.ch) to
be notified of the next install fests and our other events.

<!--
We are at your disposal between 10h and 18h in the [CM 1 106](https://plan.epfl.ch/?room=CM%2B1%2B106&dim_floor=1) for the Install Fest.
-->

In the meantime, you can pass by to one of our
[meeting](https://wiki.gnugen.ch/reunions/accueil), or have a read: for example the article about [Linux on Wikipedia](http://en.wikipedia.org/wiki/Linux).

<!--
An installation can take between about fifteen minutes and a couple of hours
depending on your machine, what you want to do with it and the problems that we
might encounter. Those willing to dive deeper into the topic can take advantage
of the knowledge of more experienced members.

A valid covid certificate is necessary to participate.

## Costs and benefits?

It's completely free (as in free beer)! You can also make a small contribution in the form of money ~~or food~~ (not this time) if you want.

## Preparation

Before coming to the Install Fest, prepare your computer so that:

- You have at least 20GB of free space
- You have an up to date backup of your computer (you should already have one)

It will make the installation faster and easier!

## What am I supposed to do now?

If you need more information, you can read the following pages:

- The [frequently asked questions page](/fr/install-fest/faq/) (in french) if you still have questions about Install Fests
- The [Wikipedia article about Linux](https://en.wikipedia.org/wiki/Linux) can give you more information about this family of operating systems
- You can also [contact us](/en/contact/) for more specific questions
-->
