---
layout: page
lang: fr
permalink: /fr/about/
---

Bienvenue sur le site de la __GNU Generation__, la commission de l'AGEPoly de promotion du [logiciel
libre](https://fr.wikipedia.org/wiki/Logiciel_libre) sur le campus de l'[EPFL](https://www.epfl.ch/). Ce site présente
notre association, les événements que nous organisons et les services que nous proposons aux étudiants et
collaborateurs.

## Le logiciel quoi ?

Selon la [Free Software Foundation](https://www.fsf.org), les logiciels libres sont ceux qui respectent les quatre
libertés fondamentales de leurs utilisateurs:

* La liberté d'exécuter le logiciel, pour tout usage.
* La liberté d'étudier le fonctionnement du logiciel, et de l'adapter à ses besoins.
* La liberté de distribuer des copies du logiciel, et donc d'aider votre voisin.
* La liberté d'améliorer le logiciel et de diffuser ses améliorations, pour le bien de toute la communauté.

Ils sont un gage de _qualité_, de _sécurité_ et de _liberté_ dans le monde de l'informatique moderne. En effet, ils sont
le produit d'une synergie entre les utilisateurs et les développeurs (ceux qui écrivent les programmes) car les
développeurs sont aussi des utilisateurs de leur programmes -- contrairement aux gros « software shops » dans lesquels
un développeur ne va jamais utiliser ce qu'il code -- et tout utilisateur qui le souhaite peut devenir développeur.
Ainsi, le code est souvent le produit d'un travail collaboratif qui assure une plus grande qualité de celui-ci que celui
de logiciels propriétaires qui n'est jamais relu et jugé par ses utilisateurs. Cette même qualité est à l'origine de la
grande sécurité offerte par le logiciel libre car de nombreux experts indépendants, n'ayant d'autres intérêts que leur
propre utilisation du logiciel, se sont penchés sur son code pour s'assurer de sa sécurité. Enfin, comme mentionné
ci-dessus, le logiciel libre assure avant tout les libertés fondamentales de son utilisateur, sortes de _Droits de
l'Homme_ de l'informatique.

De plus, comme le logiciel est fait par et pour les membres d'une communauté, il respecte totalement la vie privée de ses
utilisateurs: premièrement parce qu'il n'y a pas d'organisation avec des intérêts économiques ou politiques derrière le
logiciel et deuxièmement parce qu'un tel manquement à la décence élémentaire serait immédiatement découvert et retiré
par d'autres développeurs.

## Ça à l'air bien, mais… vous faites quoi dans tout ça ?

Tu es intéressé par le logiciel libre ? Tu trouves qu'il serait temps de prendre en main tes libertés informatiques et
la protection de ta vie privée ? Alors la _GNU Generation_ t'offre l'occasion de concrétiser ces idéaux ! Nous pouvons
te conseiller sur des choix de logiciels, t'aider à installer ou utiliser ceux-ci, ou encore te proposer des
[services](https://gnugeneration.epfl.ch/fr/services) utiles au jour le jour dans tes études et ta vie, s'appuyant sur
le libre. Notre service le plus populaire est d'aider à installer [GNU/Linux](https://getgnulinux.org/fr/linux/), un
système d'exploitation (le logiciel qui fait fonctionner ton ordinateur, comme Microsoft Windows® ou Mac OS X®)
entièrement libre qui te permettra de profiter de ton ordinateur en toute confiance et liberté; toutefois nous sommes
aussi à ta disposition pour des conseils sur de nombreux logiciels, tels que _git_, _vim_ ou _emacs_, _GPG_ et
bien d'autres ! N'hésite pas à [passer nous voir](https://plan.epfl.ch/?q=cM0415) en journée, aller à l'une de nos
[install-fests](https://wiki.gnugen.ch/if/accueil) où encore à nous
[contacter](https://gnugeneration.epfl.ch/fr/contact). Nous serons ravis de pouvoir t'aider!

## Mais alors je ne suis pas concerné si je ne suis pas un super-geek ?

__FAUX__ ! Les logiciels libres s'adressent à tout le monde, il n'est absolument pas nécessaire d'être un crack en
informatique pour les utiliser. Ils sont même souvent plus simples d'utilisation que leur contreparties propriétaires de
par leur stabilité. Par exemple, beaucoup d'entre vous utilisent probablement déjà
[VLC](https://www.videolan.org/vlc/index.fr.html) pour regarder vos films et vidéos. Eh bien, c'est un logiciel libre !
Et c'est pour cela qu'il est disponible dans autant de langues, avec tant de fonctionnalités, gratuitement et pour tous.
_Surtout_ si vous n'y connaissez rien en informatique, alors vos arrières sont beaucoup mieux protégées grâce au libre
qu'en faisant - naïvement - confiance à de grosses corporations qui ont l'argent comme seul intérêt.

## Comment peut-on vous rejoindre ?

Si tu veux t'investir plus encore dans la promotion du logiciel libre à l'EPFL, te retrouver avec d'autres gens qui
pensent comme toi pour passer des bons moments et continuer à apprendre toujours plus, tu peux nous rejoindre ! Nos
seules exigences sont d'être étudiant ou collaborateur à l'EPFL car nous sommes une commission de l'AGEPoly. Le plus
simple est de passer lors de l'une de nos [réunions](https://wiki.gnugen.ch/reunions/accueil) pour rencontrer le comité
et t'inscrire parmi nous. Ami libriste, nous t'attendons !
