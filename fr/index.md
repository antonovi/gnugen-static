---
layout: page
lang: fr
permalink: /fr/
---

Bienvenue sur le site de la __GNU Generation__, la commission de l'AGEPoly de promotion du [logiciel
libre](https://fr.wikipedia.org/wiki/Logiciel_libre) sur le campus de l'[EPFL](https://www.epfl.ch/). Plus d'informations sur nos missions [ici](/fr/about/).

# Prochains évènements

## Geekerie

Nous organisons une geekerie ce semestre, tu peux envoyer un mail
à [subscribe+annonces@lists.gnugen.ch](subscribe+annonces@lists.gnugen.ch) pour
être informé quand la date sera confirmée.

Au plaisir de te voir!
