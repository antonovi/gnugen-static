---
layout: page
lang: fr
permalink: /fr/contact/
---

## Local

Nous utilisons un local en [**CM 0 415**](https://plan.epfl.ch/?room=CM+0+415) dans lequel tu trouveras presque toujours des membres qui seront ravis de te donner plus d'informations et/ou un coup de main. Tu peux même passer pendant les week-ends!

## Chat

Nous sommes également joignable par chat dans notre salle de discussion principale qui est bridgée entre Matrix et IRC:

 * [depuis le web](https://web.libera.chat/?channel=#gnugen), ou pointez votre client IRC vers `irc.libera.chat` port 6697, #gnugen;
 * pour les chanceux qui ont déjà un serveur Matrix: [#main:gnugen.ch](https://matrix.to/#/#main:gnugen.ch).

(En tant que membre de l'association, vous avez accès à un compte de messagerie instantanée sur notre serveur Matrix.)

## Réunions hebdomadaires

Nous organisons des [réunions toutes les deux semaines](https://wiki.gnugen.ch/reunions/accueil) en cours de semestre.

## Adresses électroniques et postale

Vous pouvez nous contacter par courrier électronique à l'adresse **contact (arobase) gnugen.ch** ou chaque membre de
comité à travers:

  * **president (arobase) gnugen.ch** pour le président, actuellement *Matthieu De Beule*.
  * **vicepresident (arobase) gnugen.ch** pour le vice-président, actuellement *Ulysse Widmer*
  * **tresorier (arobase) gnugen.ch** pour la trésorière, actuellement *Haley Owsianko*.
  * **informatique (arobase) gnugen.ch** pour les responsables informatique, actuellement *Joachim Desroches* et
    *Antoine Fontaine*.

Nous disposons aussi d'une case postale:

```
AGEPoly - GNU Generation
P.a. AGEPoly
Case postale 16
CH-1015 Lausanne
```
