---
layout: page
lang: fr
permalink: /fr/services/
---

# Services

Nous proposons plusieurs services aux associations, collaborateurs et étudiants de l'EPFL, ainsi que d'autres réservés à
nos membres. Nous vous invitons à consulter notre [wiki](https://wiki.gnugen.ch/association/services) pour obtenir plus
de détails sur chacun d'eux.

## Associations

Nous sommes toujours heureus de t'aider à utiliser des logiciels libres pour ton association! Tu peux trouver plus
d'informations sur les façons de nous contacter à ce sujet [ici](https://gnugeneration.epfl.ch/fr/agep-libre/).

## Étudiants

#### Aide à l'installation de GNU/Linux

Nous sommes toujours disponibles pour aider à installer un système d'exploitation *GNU/Linux* sur votre machine. Pour ceux qui peuvent venir sur le campus, nous sommes parfois en [CM 0 415](https://plan.epfl.ch/?room=CM+0+415). Nous restons joignable par d'autres moyens, voir [contact](/fr/contact/).

<!-- Il vous
suffit pour cela de passer à notre local en journée ou de venir à l'une de nos install fests (voir les [dernières
nouvelles](/fr/news)). N'hésitez pas à [nous contacter](/fr/contact) si vous avez des questions ou que votre situation
est particulière, nous ferons de notre mieux pour vous renseigner ! -->

#### Serveurs de jeu

Nous hébergeons quelques serveurs de jeux libres. Ceux-ci sont listés sur le
[wiki](https://wiki.gnugen.ch/association/games).

## Membres

Certains de nos services ne sont accessibles qu'aux membres de l'association. Nous offrons notamment un hébergemment de
site personnel, un accès shell à un de nos serveur, de l'espace de stockage NAS, un service de synchronisation de
calendriers et un compte Matrix. Tu peux devenir membre [ici](https://wiki.gnugen.ch/association/devenir_membre).
