---
layout: page
lang: fr
permalink: /fr/install-fest/
---

# Install Fests

Ce semestre nous **avons fait** l'Install Fest le 10 octobre, en [CM 1 106](https://map.epfl.ch/?room=CM%201%20106).

![Affiche de l'Install-Fest du 10 Octobre 2021](/assets/images/posts/install-fest-fall-2021.png)

## Qu'est ce que c'est?

Une Install Fest est un évènement organisé par la [GNU Generation](/fr/) où vous trouverez des personnes prêtes et motivées
à vous aider à installer ou utiliser GNU/Linux ou tout autre logiciel libre.

Vous y trouvez également des personnes intéressées par les ordinateurs, les logiciels libres et autres sujets de geek qui se rencontrent et partagent des connaissances et des bons délires autour de petites présentations.

<!--
Parmi les autres activités pendant un install fest se trouvent:

- Signature des [clés PGP](https://gnupg.org/) ([PGP Keysigning Party](http://linuxreviews.org/howtos/gnupg/signingparty/))
- Présentations de differents logiciels libres
- ~~[Picnic Canadien](https://en.wikipedia.org/wiki/Potluck) :D~~ malheureusement pas cette année
-->

## Quand et où?

**Rendez vous le semstre prochain !** Tu peux envoyer un mail
à [annonces+subscribe@lists.gnugen.ch](annonces+subscribe@lists.gnugen.ch) pour
être informé de la prochaine install fest, et de nos autres évènements.

<!--
Nous nous tenons à disposition de 10h à 18h dans la [CM 1 106](https://plan.epfl.ch/?room=CM%2B1%2B106&dim_floor=1) pour l'Install Fest.
-->

En attendant, tu peux passer à une de nos
[réunions](https://wiki.gnugen.ch/reunions/accueil), ou te renseigner un peu
plus: par exemple avec l'article [Linux sur
Wikipedia](http://fr.wikipedia.org/wiki/Linux).

<!--
Une installation peut durer d'une quinzaine de minutes à quelques heures, en
fonction de la machine et des problèmes qui pourraient éventuellement survenir.
Ceux qui aimeraient se plonger plus profondément dans la matière peuvent
profiter du savoir des membres plus expérimentés. 

Un certificat COVID valable est obligatoire pour pouvoir participer, selon les règles en vigueur.

## Coûts et bénéfices?

C'est totalement gratuit! 
Vous pouvez cependant faire une donation monétaire ~~ou alimentaire~~ si vous le désirez.

## Préparation

Avant de venir à l'Install Fest, 
préparez votre ordinateur selon les indications suivantes:

- Vérifiez que vous avez assez d'espace disque libre sur votre ordinateur (au minimum 20Go de libre).
- Ayez une backup à jour de votre ordinateur (vous devriez déja en avoir une).

Cela facilitera l'installation et la rendra plus rapide!

## Et maintenant je fais quoi?

Pour vous renseigner un peu plus, 
vous trouvez des informations sur les pages suivants:

- Sur la page des [questions fréquentes](/fr/install-fest/faq/) si vous avez encore des questions par rapport aux Install Fests.
- L'article portant sur [Linux sur Wikipedia](http://fr.wikipedia.org/wiki/Linux) peut vous renseigner sur différents aspects de ce système d'exploitation.
- Vous pouvez aussi [nous contacter](/fr/contact/) pour des renseignements plus spécifiques.
-->
