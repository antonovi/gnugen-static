---
layout: page
lang: fr
permalink: /fr/install-fest/faq/
---

# Questions fréquemment posées

## Combien de temps dure une installation ?
Si tout va bien, 
l'installation peut se passer dans une quinzaine de minutes.
Mais ça peut toujours prendre plus de temps si quelque chose ne marche pas.
Donc comptez trois heures. 
Cela comprend le temps que nous mettrons pour partitionner votre disque dur (si ce n’est pas déjà fait), 
installer des logiciels supplémentaires de votre choix, et un petit moment pour vous familiariser avec votre nouveau système.

## Est-ce que je dois effacer Windows ?
Non, pas du tout. 
Les deux systèmes peuvent coexister sans problème.
Ce type d'installation s'appelle dual-boot 
et permet de choisir à chaque démarrage de l'ordi quel système sera booté.

## Combien de place libre dois-je avoir sur mon disque dur ?
Nous recommandons un minimum de 30 GB pour pouvoir installer GNU/Linux et avoir assez de place pour stocker quelques données et installer des logiciels.

## Est-ce que Linux tourne sur un pc portable ?
Normalement oui, 
mais si la machine est récente
il pourrait y avoir des problèmes. 
Pour en savoir plus, 
vous pouvez essayer de chercher des informations sur Internet ou [nous envoyer un e-mail](/fr/contact/#adresses-électroniques-et-postale). 
Veuillez bien indiquer le nom et type précis de votre portable.

## Quels logiciels s’installeront avec Linux ?
Il existe des milliers de logiciels disponibles sous Linux.
Cela comprend des programmes de traitement d’image, de traitement de texte, des langages de programmation, des outils Internet, des programmes multimedia, divers interfaces graphiques et même des jeux.

## Est-ce que je dois faire un backup de mes données ?
**Même sans installer Linux, c’est très recommandé.** 
Nous ferons de notre mieux pour éviter toute perte de données accidentelle, 
mais nous déclinons toute responsabilité pour vos données. 
Sur les 50 ordinateurs qui ont passés aux Install Fests précédentes on a eu un cas qui nécessitait la réinstallation de Windows. 
Heureusement cette personne avait des backups.

## Quelle est la meilleure distribution Linux ?
Pour quelqu’un qui débute, 
c’est certainement la distribution d’un ami ou de quelqu’un qui pourrait l’aider, 
au cas où ! 
La plupart de gens restent fidèles à la distribution avec laquelle ils ont commencé, 
pour la simple raison qu’ils la connaissent bien. 
En général, 
il est sage de choisir une distribution assez répandue, 
car la documentation sur Internet est alors très complète.

Si vous ne souhaitez pas une distribution spécifique, 
nous vous installerons [Ubuntu](https://ubuntu.com/download/desktop). 
Si vous voulez déjà vous faire une idée sur différentes distributions 
vous pouvez vous rendre sur le site [Distrowatch](https://distrowatch.com/?language=FR) 
présentant la pluspart des distributions 
et qui propose notamment un aperçu des [distributions principales](https://distrowatch.com/dwres.php?resource=major).

## Est-ce qu’il existe un traitement de texte sous Linux ?
Nous vous proposons d’installer LibreOffice. 
Ce programme ressemble à la suite Microsoft Office™
et est capable de modifier des documents Word™, Excel™ et Powerpoint™, 
et dispose d’un module de dessin vectoriel, de base de données, etc…

Il y a aussi LaTeX qui est la solution idéale pour vos rapports ainsi que pour tout document technique ou scientifique. 
Il nécessite toutefois un certain apprentissage.
