---
layout: page
lang: fr
permalink: /fr/agep-libre/
---

# Associations, commissions: libérez vos outils informatiques!

Les logiciels privateurs ne respectent pas vos libertés, souvent ne respectent pas votre droit humain de vie privée, vous entravent en limitant les utilisations qui sont permises, avec les soucis de licenses, vous empêchent de les étudier et de les améliorer,...

Pour en savoir plus, référez-vous à [la page d'explication de la FSFE](https://fsfe.org/freesoftware/freesoftware.fr.html), ou allez regarder sur [notre page de présentation](/fr/about/).

Il est important de ne pas dépendre d'outils propriétaires et éthiquement problématiques dans le contexte de la vie associative. C'est pourquoi nous proposons notre aide aux associations et commissions de l'AGEPoly pour toute question ou conseil que vous pourrez avoir sur ce sujet.

N'hésitez donc pas à nous contacter et à rejoindre notre groupe de messagerie instantanée dédié au sujet si ça vous intéresse!

## 📨 Courrier électronique (e-mail)

Vous pouvez nous contacter par courrier électronique, référez-vous à [notre page de contact](/fr/contact/).

## 💬 Groupe de messagerie instantanée (Telegram ou Matrix)

Vous pouvez nous rejoindre dans un groupe dédié à ce sujet, sur **Telegram** ou **Matrix** (la conversation est synchronisée sur les deux plateformes, donc pas besoin de venir sur les deux).

Pour rejoindre sur **Telegram**: [cliquez ici](https://t.me/joinchat/HtffmozpNtF6_I_7)

Matrix est une plateforme de messagerie instantanée, qui est décentralisée et libre.  
Vous pouvez rejoindre notre conversation depuis n'importe quel serveur Matrix.
Si vous n'avez pas encore de compte ou application Matrix, vous devriez être guidé dans la procédure en cliquant sur le lien de notre canal (ci-dessous).

Pour rejoindre sur **Matrix** (à préférer plutôt que Telegram): [#ageplibre:gnugen.ch](https://matrix.to/#/#ageplibre:gnugen.ch)


